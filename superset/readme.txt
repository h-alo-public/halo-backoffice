[Service]

http://localhost:8188


[Credentials]

login: admin
password: admin


[Troubleshooting]

Change permissions for "halo-backoffice/datalake/analysis/superset/sql" folder in your host (with Linux OS) using command:

chmod -R 777 sql

This folder, containing sql scripts, is mounted as a volume to folder "/docker-entrypoint-initdb.d" (with 777 permissions) in mysql container.
