#!/bin/bash
# wait-and-init.sh

set -e

export PGPASSWORD=${POSTGRES_PASSWORD}

PSQL_PREFIX="psql -h ${POSTGRES_HOST} -p 5432 -d ${POSTGRES_DB} -U ${POSTGRES_USER} --no-password -tAc "
PSQL_SUFIX=" 2>/dev/null"

PSQL_CHECK_CONN="${PSQL_PREFIX}'SELECT 1'${PSQL_SUFIX}"

echo "Checking: $PSQL_CHECK_CONN"
until eval ${PSQL_CHECK_CONN}; do
    echo "wait-and-init.sh: POSTGRES is unavailable - sleeping" >&2
    sleep 5
done

echo "wait-and-init.sh: POSTGRES is up - executing command(s)" >&2

PSQL_CHECK_DB="${PSQL_PREFIX}\"SELECT COUNT(*) FROM pg_tables WHERE schemaname = 'public' AND tablename = 'measurement_series'\"${PSQL_SUFIX}"

if [ `eval ${PSQL_CHECK_DB}` -eq 0 ]; then
    echo "wait-and-init.sh: superset fab create-admin --username admin --password admin --firstname Superset --lastname Admin --email admin@superset.com" >&2 && superset fab create-admin --username ${SUPERSET_USER} --password ${SUPERSET_PASSWORD} --firstname Superset --lastname Admin --email ${SUPERSET_USER_EMAIL}
    echo "wait-and-init.sh: superset db upgrade" >&2 && superset db upgrade
    echo "wait-and-init.sh: superset init" >&2 && superset init
    echo "wait-and-init.sh: measurement database initialization" >&2 && psql -h ${POSTGRES_HOST} -p 5432 -d ${POSTGRES_DB} -U ${POSTGRES_USER} --no-password -q -f ${SUPERSET_DOCKER_DIR}/sql/postgres-init-db.sql
    echo "wait-and-init.sh: superset import-datasources" >&2 && superset import-datasources -p $SUPERSET_DOCKER_DIR/import/superset-datasources.yaml
    echo "wait-and-init.sh: superset set-database-uri" >&2 && superset set-database-uri -d "postgres_ds" -u "postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:5432/${POSTGRES_DB}"
else
    echo "wait-and-init.sh: measurement database is already initialized" >&2
fi

