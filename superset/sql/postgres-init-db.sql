CREATE TABLE measurement_series (
	site_id UUID NOT NULL,
	session_id UUID NOT NULL,
	measurement_time TIMESTAMP NOT NULL,
	value DOUBLE PRECISION NOT NULL
);

--CREATE INDEX series_idx ON measurement_series(site_id, measurement_time, session_id);
