from celery import Celery
from minio import Minio
import random, json, datetime, random

siteUUIDs = ['31048141-2AAB-4931-B86B-47C56ABFEF28','E18043A5-59FB-4522-A439-E82F68F41198','54E512C8-3589-4746-8E33-C8DCBA23AB59','4E0E0F5F-4E9E-4AF9-8001-05730F8E4E06','9CF5E1D5-8A91-4184-88EE-006C1DBDED36','FA9AE6F4-656B-4F12-AC38-59FA404A2C14','C16BD992-AFAC-4BE2-A2FD-2B8E620912AC','CC349FF6-0BC1-4D8E-99F8-5C014AB6AE06','DB778397-830D-47C1-AC71-7AD7037271EA','63AED202-3034-4207-921F-FBFAF6E6AB4E']
siteUUID = siteUUIDs[random.randint(0, 9)]
objectName = 'obj' + str(random.randint(100000, 999999))

def putJSONToMinio():
	jd = []
	for i in range(10):
		jd.append({
		"timestamp": datetime.datetime.now().timestamp() + i,
		"seriesId": "series-0001",
		"cartridgeID": "cart-0x0x",
		"opmID": "opm1",
		"baseValue": 100.0,
		"variation": 3,
		"normalizedValue": random.uniform(80, 120),
		"temperature": 21.3,
		"batteryLevels": 80
		})
	with open("test_json.txt", "w") as write_file:
		json.dump(jd, write_file)
	
	minio_client = Minio('localhost:9000', access_key='minioadmin', secret_key='minioadmin', secure=0)
	bucketName = siteUUID.lower()
	found = minio_client.bucket_exists(bucketName)
	if not found:
		print("Creating '"+ bucketName +"'")		
		minio_client.make_bucket(bucketName)
		print("Bucket '"+ bucketName +"' created")		
	minio_client.fput_object(bucketName, objectName, "test_json.txt")
	print("Object '"+ objectName +"' for site '" + siteUUID + "' uploaded")	


def startCelleryTask():
	celery = Celery(
		broker='amqp://rabbitadmin:rabbitadmin@localhost:5672'
	)
	print("calling task process_json")
	celery.send_task("process_json", kwargs={'measurement_id': objectName, 'sender_name': 'SNDR', 'site_uuid': siteUUID, 'device_id': 'DVCE', 'cartridge_id': 'CRTRDG'})
	print("done")

def main():
	putJSONToMinio()
	startCelleryTask()
	
if __name__ == "__main__":
    try:
        main()
    except S3Error as exc:
        print("error occurred.", exc)
