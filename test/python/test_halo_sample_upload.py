import random, json, datetime, requests
from requests_toolbelt.multipart.encoder import MultipartEncoder

# Access data
keycloak_url = "http://halo.7bulls.com:8180/auth/realms/halo-keycloak/protocol/openid-connect/token"
keycloak_user = "user1"
keycloak_password = "user1"
backoffice_url = "http://halo.7bulls.com:8081"

# Sample data
siteNames = ['Milk-01','Milk-02','Site-03','Site-04']
siteName = siteNames[random.randint(0, 3)]
deviceName = 'SomeDevice'
cartridgeName = 'TestCartridge'

def createSampleFile():
	jd = []
	for i in range(10):
		jd.append({
		"timestamp": datetime.datetime.now().timestamp() + i,
		"seriesId": "series-0001",
		"cartridgeID": "cart-0x0x",
		"opmID": "opm1",
		"baseValue": 100.0,
		"variation": 3,
		"normalizedValue": random.uniform(80, 120),
		"temperature": 21.3,
		"batteryLevels": 80
		})
	with open("sample.json", "w") as write_file:
		json.dump(jd, write_file)

def callAPI():
	print("Obtaining token")
	key_req = {"username": keycloak_user, "password": keycloak_password, "grant_type": "password", "client_id": "springboot-keycloak"}
	response = requests.post(keycloak_url, data=key_req)
	token = ""
	if response.status_code == 200:
		print("Upload file")
		token = response.json()["access_token"]
		api_url = backoffice_url + "/api/measurement"
		with open('sample.json') as fp:
			file_data = fp.read()
			fields={ 
				'sitename': siteName,
				'device': deviceName,
				'cartridge': cartridgeName,
				'filedata': ('sample.json', file_data, 'text/plain'),
			}
			mp_encoder = MultipartEncoder(fields=fields)
			hdrs = {"Authorization": "Bearer " + token, "Content-Type": mp_encoder.content_type}
			response = requests.post(api_url, data=mp_encoder, headers=hdrs)
			if response.status_code == 200:
				print("File uploaded")	
			else:
				print("Error uploading flie (" + str(response.status_code) + "): " + str(response.json()))
	else:
		print("Error obtaining token from Keycloak (" + str(response.status_code) + "): " + str(response.json()))

def main():
	createSampleFile()
	callAPI()
	
if __name__ == "__main__":
    try:
        main()
    except S3Error as exc:
        print("error occurred.", exc)
