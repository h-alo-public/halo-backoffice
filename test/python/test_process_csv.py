from celery import Celery
from minio import Minio
import random

siteUUID = '31048141-2AAB-4931-B86B-47C56ABFEF28'
objectName = 'obj' + str(random.randint(100000, 999999))
bucketName = siteUUID.lower()

def putCSVToMinio():
	minio_client = Minio('localhost:9000', access_key='minioadmin', secret_key='minioadmin', secure=0)	
	found = minio_client.bucket_exists(bucketName)
	if not found:
		print("Creating '"+ bucketName +"'")		
		minio_client.make_bucket(bucketName)
		print("Bucket '"+ bucketName +"' created")		
	else:
		print("Bucket '"+ bucketName +"' already exists")
		
	minio_client.fput_object(bucketName, objectName, "test_csv.txt")
	print("Object '"+ objectName +"' uploaded")	


def startCelleryTask():
	celery = Celery(
		broker='amqp://rabbitadmin:rabbitadmin@localhost:5672'
	)
	print("calling task process_csv")
	celery.send_task("process_csv", kwargs={'measurement_id': objectName, 'sender_name': 'SNDR', 'site_uuid': siteUUID, 'device_id': 'DVCE', 'cartridge_id': 'CRTRDG'})
	print("done")

def main():
	putCSVToMinio()
	startCelleryTask()
	
if __name__ == "__main__":
    try:
        main()
    except S3Error as exc:
        print("error occurred.", exc)
