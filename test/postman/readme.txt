[Settings]

#Before running tests in Postman add this "ip-hostname" mapping in "/etc/hosts" (Linux) or "C:\Windows\System32\drivers\etc\hosts" (Windows)

127.0.0.1	keycloak-svc

[Tests]

A. Manual

Import JSON tests collection to "Postman" application with GUI and click "Run" button.

B. Automatic

Install packages:
npm install -g newman
npm install -g newman-reporter-htmlextra

Check installed versions:
newman --version
newman-reporter-htmlextra --version

Update packages:
npm update -g newman
npm update -g newman-reporter-htmlextra

Run tests without/with html report:
newman run "SpringBoot Halo OpenAPI Tests (Auto).postman_collection.json"
newman run "SpringBoot Halo OpenAPI Tests (Auto).postman_collection.json" --reporters cli,htmlextra
newman run "SpringBoot Halo OpenAPI Tests (Auto).postman_collection.json" --reporters cli,htmlextra --working-dir .
