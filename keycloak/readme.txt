[Service]

http://localhost:8180
http://keycloak-svc:8180

Note: Wait about 1-2 minute(s) for starting the Keycloak (JBoss) server.


[Hosts]

127.0.0.1	keycloak-svc

Note: Before using "keycloak-svc" hostname add above "ip-hostname" mapping in "/etc/hosts" (Linux) or "C:\Windows\System32\drivers\etc\hosts" (Windows).
This hostname and its port are inserted in JWT (Json Web Token) for verification (i.e. in "iss" payload field) and used by Spring application
for accessing Keycloak server (i.e. via "keycloak.auth-server-url" property). In both cases hostnames and ports associated with them must be the same!


[Credentials]

These credentials are used for an administrator account:
login: admin
password: admin


[Troubleshooting]

If there is a port conflict with other docker containers ports exposed in host, please change the following lines in "docker-compose.yml" file according to this example:

A. Before:
keycloak-svc:
  ...
  command: ["-Djboss.http.port=8080", "-Djboss.socket.binding.port-offset=0"]
  ports:
    - 8080:8080

springboot-svc:
  ...
  environment:
    SPRING_APPLICATION_JSON: '{"keycloak.auth-server-url": "http://keycloak-svc:8080/auth"}'

B. After:
keycloak-svc:
  ...
  command: ["-Djboss.http.port=8180", "-Djboss.socket.binding.port-offset=0"]
  ports:
    - 8180:8180

springboot-svc:
  ...
  environment:
    SPRING_APPLICATION_JSON: '{"keycloak.auth-server-url": "http://keycloak-svc:8180/auth"}'

Note: Host Port (e.g. used in Postman) and Container Port (e.g. used in Spring application) must be the same. For verification purposes. In order to avoid conflict with
default Keycloak port (i.e. 8080) in host environment, change server port in Keycloak container using "jboss.http.port" or "jboss.socket.binding.port-offset" properties
and in Spring container using "keycloak.auth-server-url" property. In fact, all four port numbers in Keycloak and Spring services settings must be always changed to the same new value!


[Settings]

[Realm Settings] => [General] => [Name] = halo-keycloak
[Clients] => [Lookup] => [Client ID] = springboot-keycloak
[Clients] => [Settings] => [Client ID] = springboot-keycloak
[Clients] => [Settings] => [Access Type] = public
[Clients] => [Settings] => [Standard Flow Enabled] = ON
[Clients] => [Settings] => [Direct Access Grants Enabled] = ON
[Clients] => [Settings] => [Root URL] = http://localhost:8081
[Clients] => [Settings] => [Valid Redirect URIs] = http://localhost:8081/*
[Clients] => [Settings] => [Admin URL] = http://localhost:8081
[Clients] => [Settings] => [Web Origins] = http://localhost:8081
[Roles] => [Realm Roles] => [Role Name] = role1
[Roles] => [Realm Roles] => [Role Name] = role2
[Users] => [Lookup] => [ID] = 2c9a99e8-1ea9-41f6-8f10-d0ff15529849
[Users] => [Details] => [Username] = user1
[Users] => [Credentials] => [Password] = user1
[Users] => [Role Mappings] => [Realm Roles] = -
[Users] => [Role Mappings] => [Available Roles] = role1
[Users] => [Role Mappings] => [Assigned Roles] = role1
[Users] => [Role Mappings] => [Effective Roles] = role1
[Users] => [Lookup] => [ID] = c2ae33c1-6a08-4869-9138-1a25b8183c91
[Users] => [Details] => [Username] = user2
[Users] => [Credentials] => [Password] = user2
[Users] => [Role Mappings] => [Realm Roles] = -
[Users] => [Role Mappings] => [Available Roles] = role2
[Users] => [Role Mappings] => [Assigned Roles] = role2
[Users] => [Role Mappings] => [Effective Roles] = role2

Note: Some values (e.g. IDs) are generated automatically or visible only during configuration process (e.g Roles).
Initial (example) settings are loaded from "halo-keycloak-config.json" file. Modified settings are preserved in "keycloak-data" docker volume.


[Export]

If you want to export Keycloak realm configuration use the following command:

docker exec -it keycloak-cntr /opt/jboss/keycloak/bin/standalone.sh --server-config=standalone-default.xml -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.realmName=halo-keycloak -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Dkeycloak.migration.file=/tmp/halo-keycloak-export.json
docker cp keycloak-cntr:/tmp/halo-keycloak-export.json halo-keycloak-config.json

[Swagger]

Swagger UI URL:
http://localhost:8081/swagger-ui/index.html
