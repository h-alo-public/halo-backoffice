resource "aws_db_subnet_group" "halo" {
  name       = "halo"
  subnet_ids = module.vpc.public_subnets

  tags = {
    Name = "halo"
  }
}

resource "aws_security_group" "rds" {
  name   = "halo_rds"
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "halo_rds"
  }
}

resource "aws_db_parameter_group" "halo" {
  name   = "halo"
  family = "postgres13"

  parameter {
    name  = "log_connections"
    value = "1"
  }
}

resource "aws_db_instance" "halo" {
  identifier             = "halo"
  instance_class         = "db.t3.micro"
  allocated_storage      = 5
  engine                 = "postgres"
  engine_version         = "13.4"
  username               = "halo_admin"
  password               = var.db_password
  db_subnet_group_name   = aws_db_subnet_group.halo.name
  vpc_security_group_ids = [aws_security_group.rds.id]
  parameter_group_name   = aws_db_parameter_group.halo.name
  publicly_accessible    = true
  skip_final_snapshot    = true
}

resource "null_resource" "db_setup" {

  depends_on = [aws_db_instance.halo, aws_security_group.rds]

  provisioner "local-exec" {

    command = "psql -h ${aws_db_instance.halo.address} -p 5432 -U \"${aws_db_instance.halo.username}\" -d \"postgres\" -f \"./rds-init.sql\""

    environment = {
      PGPASSWORD = "${var.db_password}"
    }
  }
}
