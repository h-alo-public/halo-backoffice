# Sub-dirs

./terraform - terraform code to create infrastructure for the project:
- EKS ("halo-eks-cluster")
- RDS ("halo")
- AWS Load Balancer Controller

./halo-chart - helm chart containing project's components:
- Backoffice
- Webbapp
- Keycloak
- Ingress

./docker - test environment using docker-compose with Postgres included 

# Prerequisities

NOTE: Before you start following tools need to be installed and configured in the system:
- kubectl
- helm
- aws
- terraform

# Brief instruction

1. To create infrastructure with terraform (in ./terraform dir)
> terraform init
> terraform apply

2. To configure kubectl.
> aws eks update-kubeconfig --region "eu-central-1" --name "halo-eks-cluster"

3. To connect to DB (load initial data or whatever).
> psql -h halo.<aws_rds_id>.<aws_zone>.rds.amazonaws.com -p 5432 -d "postgres" -U "halo_admin"
> Password: nPQeFSneZ8fZS9e7 (change in terraform scripts)

4. Before using the helm chart - update "host" and "postgresHost" in ../halo-chart/values.yaml.

5. To install application with helm.
> ./rebuildChart.sh
> ./installChart.sh

6. And finally, to shut it all down...
> terraform destroy

