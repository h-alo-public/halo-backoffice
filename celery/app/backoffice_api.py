import os, json, datetime, requests

#### Creates Result entity calling Backoffice API (authorize with Keycloak first). Returns site name.

class BackofficeAPI:
	
	headers = ""
	def __init__(self):
		self.authorize()
	
	def authorize(self):
		response = requests.post(os.environ['KEYCLOAK_URL'],
			data={
				"username": os.environ['KEYCLOAK_USER'],
				"password": os.environ['KEYCLOAK_PASSWORD'],
				"grant_type": "password",
				"client_id": "springboot-keycloak"
			})
		if response.status_code == 200:
			self.headers = {"Authorization": "Bearer " + response.json()["access_token"]}
		else:
			raise Exception("Error obtaining token from Keycloak (" + str(response.status_code) + "): " + str(response.json()))

	def createResult(self, result, measurement_id, sender_name, site_uuid, device_id, cartridge_id):
		# First fetch the stie name
		api_url = os.environ['BACKOFFICE_URL'] + "/api/sites/" + site_uuid
		response = requests.get(api_url, headers=self.headers)
		if response.status_code == 200:
			site_name = response.json().get("site")["name"]
		else:
			raise Exception("Site " + site_uuid + " not found (" + str(response.status_code) + "): " + str(response.json()))
		# And then create result
		api_url = os.environ['BACKOFFICE_URL'] + "/api/sites/" + site_uuid + "/results"
		response = requests.post(api_url, headers=self.headers, json={
			"resultTS": int(datetime.datetime.now().timestamp()),
			"contributor": sender_name,
			"device": device_id,
			"cartridge": cartridge_id,
			"result": result,
			"comment": "Measurement file: " + measurement_id
			})
		if response.status_code != 200:
			raise Exception("Error creating Result in Backoffice (" + str(response.status_code) + "): " + str(response.json()))
		return site_name

