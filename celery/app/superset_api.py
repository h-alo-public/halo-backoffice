import os, json, datetime, requests

#### Create Superset chart in site's dashboard (create dashboard too if not present)

class SupersetAPI:

	superset_host = os.environ['SUPERSET_HOST']
	session = requests.Session()
	headers = ""

	def __init__(self):
		self.authorize()

	def authorize(self):
		# Fetch CSRF token first
		csrf_response = self.session.get(f"http://{self.superset_host}/api/v1/security/csrf_token/")
		csrf_token = csrf_response.json().get("result")
		data = {
			'username': os.environ['SUPERSET_USER'],
			'password': os.environ['SUPERSET_PASSWORD'],
			'csrf_token': csrf_token}
		self.session.post(f'http://{self.superset_host}/login/', data=data)
		self.headers = {
			"X-CSRF-Token": csrf_token,
			"Referer": f'http://{self.superset_host}/api/v1'}
		
	def findDashboardId(self, dashboard_title):
		query = {"filters": [{"col": "dashboard_title", "opr": "eq", "value": dashboard_title }]}
		params = {"q": json.dumps(query)}
		response = self.session.get(f'http://{self.superset_host}/api/v1/dashboard', params=params)
		rjson = response.json()
		if response.status_code == 200:
			if rjson.get("count") == 0:
				return ""
			else:
				return rjson.get("result")[0]["id"]
		else:
			raise Exception("Error finding Dashboard (" + str(response.status_code) + "): " + str(response.json()))	
	
	def createDashboard(self, dashboard_title):
		dashboard = {
			"certification_details": None,
			"certified_by": None,
			"css": "",
			"dashboard_title": dashboard_title,
			"json_metadata": "{\"timed_refresh_immune_slices\": [], \"expanded_slices\": {}, \"refresh_frequency\": 0, \"color_scheme\": \"\", \"label_colors\": {}}",
			"owners": [],
			"position_json": "",
			"published": True,
			"roles": [],
			"slug": None,
		}
		response = self.session.post(f'http://{self.superset_host}/api/v1/dashboard', json=dashboard, headers=self.headers)
		if response.status_code == 201:
			return response.json().get("id")
		else:
			raise Exception("Error creating Dashboard (" + str(response.status_code) + "): " + str(response.json()))	

	def findDatasourceId(self):
		query = {"filters": [{"col": "table_name", "opr": "eq", "value": "measurement_series" }]}
		params = {"q": json.dumps(query)}
		response = self.session.get(f'http://{self.superset_host}/api/v1/dataset', params=params)
		if response.status_code == 200:
			return response.json().get("result")[0]["id"]
		else:
			raise Exception("Error finding Datasource (" + str(response.status_code) + "): " + str(response.json()))	

	def createChart(self, session_id, dashboard_title, slice_name):
		dashboard_id = self.findDashboardId(dashboard_title)
		if dashboard_id == "":
			dashboard_id = self.createDashboard(dashboard_title)
		datasource_id = self.findDatasourceId()
		chart = {
			"datasource_id" : datasource_id,
			"datasource_type" : "table",			
			"cache_timeout": None,
			"certification_details": None,
			"certified_by": None,
			"dashboards": [ dashboard_id ],
			"description": None,
			"owners": [],
			"params": "{ \"adhoc_filters\": [ { \"clause\": \"WHERE\", \"comparator\": \"" + str(session_id) + "\", \"expressionType\": \"SIMPLE\", \"isExtra\": false, \"isNew\": false, \"operator\": \"==\", \"operatorId\": \"EQUALS\", \"sqlExpression\": null, \"subject\": \"session_id\" } ], \"annotation_layers\": [], \"color_scheme\": \"supersetColors\", \"comparison_type\": \"values\", \"datasource\": \"3__table\", \"extra_form_data\": {}, \"forecastInterval\": 0.8, \"forecastPeriods\": 10, \"granularity_sqla\": \"measurement_time\", \"groupby\": [ \"session_id\" ], \"legendOrientation\": \"top\", \"legendType\": \"scroll\", \"markerSize\": 6, \"metrics\": [ { \"aggregate\": \"AVG\", \"column\": { \"certification_details\": null, \"certified_by\": null, \"column_name\": \"value\", \"description\": null, \"expression\": null, \"filterable\": true, \"groupby\": true, \"id\": 12, \"is_certified\": false, \"is_dttm\": false, \"python_date_format\": null, \"type\": \"DOUBLE\", \"type_generic\": 0, \"verbose_name\": null, \"warning_markdown\": null }, \"expressionType\": \"SIMPLE\", \"hasCustomLabel\": false, \"isNew\": false, \"label\": \"AVG(value)\", \"sqlExpression\": null } ], \"only_total\": true, \"opacity\": 0.2, \"order_desc\": true, \"rich_tooltip\": true, \"row_limit\": 10000, \"seriesType\": \"line\", \"slice_id\": 1, \"time_grain_sqla\": null, \"time_range\": \"No filter\", \"tooltipTimeFormat\": \"smart_date\", \"viz_type\": \"echarts_area\", \"x_axis_time_format\": \"smart_date\", \"x_axis_title_margin\": 15, \"y_axis_bounds\": [ null, null ], \"y_axis_format\": \"SMART_NUMBER\", \"y_axis_title_margin\": 15, \"y_axis_title_position\": \"Left\"}",
			"query_context": None,
			"slice_name": slice_name,
			"viz_type": "echarts_area"
		}
		response = self.session.post(f'http://{self.superset_host}/api/v1/chart', json=chart, headers=self.headers)
		if response.status_code == 201:
			print("Chart created (id=" + str(response.json().get("id")) + ")")	
		else:
			raise Exception("Error creating Chart (" + str(r.status_code) + "): " + str(r.json()))

