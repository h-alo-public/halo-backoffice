from minio import Minio
import csv, os, json, datetime, requests
import psycopg2, uuid

#### Parsing file retrieved from Minio and saving it's data in Superset DB
	
CSV_FILED_NV = 'normalized value'
CSV_FILED_TS = 'timestamp'

JSON_FILED_NV = 'normalizedValue'
JSON_FILED_TS = 'timestamp'	
	
class MinioParser:

	def parseDataFromMinio(self, datatype, site_uuid, measurement_id, session_id):
		minioClient = Minio(os.environ['MINIO_HOST'],  
			access_key=os.environ['MINIO_ACCESS_KEY'],  
			secret_key=os.environ['MINIO_SECRET_KEY'],  
			secure=int(os.getenv('MINIO_SECURE', '0')))
		response = 0
		values = []
		try:
			response = minioClient.get_object(site_uuid.lower(), measurement_id)
			conn = psycopg2.connect(
				database=os.environ['SUPERSET_DB'],
				user=os.environ['SUPERSET_DB_USER'],
				password=os.environ['SUPERSET_DB_PASSWORD'],
				host=os.environ['SUPERSET_DB_HOST'],
				port="5432")
			cursor = conn.cursor()
			if (datatype == 'csv'):
				values = self.parseCSV(response, cursor, session_id, site_uuid)
			elif (datatype == 'json'):
				values = self.parseJSON(response, cursor, session_id, site_uuid)
			else:
				raise Exception("Unsupported datatype: " + datatype)
			conn.commit()
			conn.close()
		finally:
			if response:
				response.close()
				response.release_conn()
		return values	
		
	def parseCSV(self, data, cursor, session_id, site_uuid):
		values = []
		reader = csv.reader(LineReader(data), delimiter=',', quotechar='"')
		nvIdx = 0
		tsIdx = 0
		line = 0
		for row in reader:
			if (line == 0):
				nvIdx = row.index(CSV_FILED_NV)
				tsIdx = row.index(CSV_FILED_TS)
			else:
				if len(row) > 0:
					values.append(float(row[nvIdx]))
					self.saveMeasurement(cursor, session_id, site_uuid, float(row[nvIdx]), int(row[tsIdx]))
			line += 1
		return values

	def parseJSON(self, data, cursor, session_id, site_uuid):
		values = []
		chunks = []
		for buf in data.stream():
			chunks.append(buf)
		content = b''.join(chunks).decode('utf-8')
		jl = json.loads(content)
		for jo in jl:
			values.append(jo[JSON_FILED_NV])
			self.saveMeasurement(cursor, session_id, site_uuid, jo[JSON_FILED_NV], jo[JSON_FILED_TS])
		return values
		
	def saveMeasurement(self, cursor, session_id, site_uuid, value, timestamp):
		cursor.execute("INSERT INTO measurement_series (site_id,session_id,measurement_time,value) VALUES ('" + site_uuid + "','" + str(session_id) + "','" + datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%S.%f') + "'," + str(value) + ")")
	
#### Helper class for parsing CSV - iterates over lines of the source

class LineReader:
	chunks = []
	buf = ""
	bufSize = 1024
	empty = False
	def __init__(self, source):
		self.source = source
	def __iter__(self):
		return self
	def __next__(self):
		if self.empty:
			raise StopIteration 
		eol = self.buf.find('\n')
		if (eol >= 0):
			res = self.buf[:eol]
			self.buf = self.buf[eol+1:]
			return res
		self.buf = self.source.read(self.bufSize).decode('utf-8')
		while len(self.buf) > 0:
			eol = self.buf.find('\n')
			if (eol > 0):
				self.chunks.append(self.buf[:eol])
				res = ''.join(self.chunks)
				self.chunks.clear()
				self.buf = self.buf[eol+1:]
				return res
			else:
				self.chunks.append(self.buf)
				self.buf = self.source.read(self.bufSize).decode('utf-8')
		self.empty = True
		return ''.join(self.chunks)
