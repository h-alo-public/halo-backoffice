from celery import Celery
from backoffice_api import BackofficeAPI
from superset_api import SupersetAPI
from minio_parser import MinioParser
import uuid

app = Celery()
app.conf.beat_schedule = {
  # no scheduled tasks 
}

#### Celery tasks

@app.task(bind=True, name='process_csv')
def process_csv(self, measurement_id, sender_name, site_uuid, device_id, cartridge_id):  
	print("process_csv started for " + measurement_id + " in " + site_uuid)
	process('csv', measurement_id, sender_name, site_uuid, device_id, cartridge_id)
	
@app.task(bind=True, name='process_json')
def process_json(self, measurement_id, sender_name, site_uuid, device_id, cartridge_id):  
	print("process_json started for " + measurement_id + " in " + site_uuid)
	process('json', measurement_id, sender_name, site_uuid, device_id, cartridge_id)

#### Main procedure

def process(datatype, measurement_id, sender_name, site_uuid, device_id, cartridge_id):
	session_id = uuid.uuid1()
	# Retrieve data from minio and store raw values in Superset DB
	values = MinioParser().parseDataFromMinio(datatype, site_uuid, measurement_id, session_id)
	if len(values) == 0:
		raise Exception("No values to process!")
	# Process it somehow
	result = processValues(values)
	# Create Backoffice result entity
	site_name = BackofficeAPI().createResult(result, measurement_id, sender_name, site_uuid, device_id, cartridge_id)
	# Create Superset chart
	SupersetAPI().createChart(session_id, "Site measurements: " + site_name, measurement_id)

#### Abstract data processing - curently it's just an average value	
	
def processValues(values):
	print("DEBUG Processing data")
	print('DEBUG values: '+ str(values))
	result = sum(values) / len(values)
	print('DEBUG result: ' + str(result))
	return result
	

	

		
