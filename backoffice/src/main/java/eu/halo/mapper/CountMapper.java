package eu.halo.mapper;

import java.math.BigDecimal;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import eu.halo.openapi.openmodels.CountResponse;

@Mapper
public interface CountMapper extends APITimestamp {

	public static CountMapper INSTANCE = Mappers.getMapper(CountMapper.class);

	@Mapping(target = "count", expression = "java(getBigDecimal(count))")
	@Mapping(target = "timestamp", expression = "java(getFormattedOffsetTimestamp())")
	CountResponse mapCountToResponse(Long count);

	default BigDecimal getBigDecimal(Long value) {
		return value == null ? null : BigDecimal.valueOf(value);
	}
}
