package eu.halo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import eu.halo.openapi.openmodels.MeasurementInfo;

@Mapper
public interface MeasurementInfoMapper extends APITimestamp {

	public static MeasurementInfoMapper INSTANCE = Mappers.getMapper(MeasurementInfoMapper.class);

	MeasurementInfo mapStringsToMeasurementInfo(String sitename, String device, String cartridge, String filename);

}
