package eu.halo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import eu.halo.openapi.openmodels.UserInfo;
import eu.halo.openapi.openmodels.UserInfoResponse;
import eu.halo.service.util.UserData;

@Mapper
public interface UserInfoMapper extends APITimestamp {

	public static UserInfoMapper INSTANCE = Mappers.getMapper(UserInfoMapper.class);

	@Mapping(source = "name", target = "name")
	@Mapping(source = "adminFlag", target = "isAdmin")
	@Mapping(source = "organizationId", target = "organizationId")
	UserInfo mapEntityToApi(UserData userData);

	@Mapping(source = "name", target = "userinfo.name")
	@Mapping(source = "adminFlag", target = "userinfo.isAdmin")
	@Mapping(source = "organizationId", target = "userinfo.organizationId")
	@Mapping(target = "timestamp", expression = "java(getFormattedOffsetTimestamp())")
	UserInfoResponse mapEntityToResponse(UserData userData);

}
