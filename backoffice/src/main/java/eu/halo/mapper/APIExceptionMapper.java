package eu.halo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.http.HttpStatus;

import eu.halo.openapi.openmodels.APIExceptionResponse;
import eu.halo.service.util.APIException;

@Mapper
public interface APIExceptionMapper extends APITimestamp {

	public static APIExceptionMapper INSTANCE = Mappers.getMapper(APIExceptionMapper.class);

	@Mapping(target = "errorType", expression = "java(exception.getStatus().getReasonPhrase())")
	@Mapping(target = "errorCode", expression = "java(String.valueOf(exception.getStatus().value()))")
	@Mapping(source = "message", target = "errorMessage")
	@Mapping(target = "timestamp", expression = "java(getFormattedOffsetTimestamp())")
	APIExceptionResponse mapEntityToResponse(APIException exception);

	@Mapping(target = "errorType", expression = "java(getStatus().getReasonPhrase())")
	@Mapping(target = "errorCode", expression = "java(String.valueOf(getStatus().value()))")
	@Mapping(target = "errorMessage", expression = "java(exception.getMessage())")
	@Mapping(target = "timestamp", expression = "java(getFormattedOffsetTimestamp())")
	APIExceptionResponse mapEntityToResponse(Exception exception);

	default HttpStatus getStatus() {
		return HttpStatus.BAD_REQUEST;
	}
}
