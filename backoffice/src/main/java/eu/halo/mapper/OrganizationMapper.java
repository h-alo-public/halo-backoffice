package eu.halo.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import eu.halo.model.OrganizationEntity;
import eu.halo.openapi.openmodels.Organization;

@Mapper
public interface OrganizationMapper extends APITimestamp {

	public static OrganizationMapper INSTANCE = Mappers.getMapper(OrganizationMapper.class);

	@Mapping(source = "organization", target = "name")
	@Mapping(source = "id", target = "organizationId")
	@Mapping(target = "createdTS", expression = "java(getOffsetTimestamp(organizationEntity.getCreated()))")
	@Mapping(target = "updatedTS", expression = "java(getOffsetTimestamp(organizationEntity.getUpdated()))")
	Organization mapEntityToApi(OrganizationEntity organizationEntity);

	List<Organization> mapEntityToApi(List<OrganizationEntity> organizationEntityList);

}
