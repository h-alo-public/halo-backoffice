package eu.halo.mapper;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public interface APITimestamp {

	ZoneOffset DATABASE_ZONE = ZoneOffset.UTC;
	DateTimeFormatter DATABASE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

	ZoneId OPENAPI_ZONE = ZoneId.of("CET");
	DateTimeFormatter OPENAPI_FORMATTER = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSSXXXXX");

	default LocalDateTime getLocalTimestamp() {
		return LocalDateTime.now(DATABASE_ZONE);
	}

	default OffsetDateTime getOffsetTimestamp() {
		return LocalDateTime.now(DATABASE_ZONE).atZone(DATABASE_ZONE).withZoneSameInstant(OPENAPI_ZONE)
				.toOffsetDateTime();
	}

	default LocalDateTime getLocalTimestamp(OffsetDateTime offsetDateTime) {
		return offsetDateTime == null ? null : offsetDateTime.withOffsetSameInstant(DATABASE_ZONE).toLocalDateTime();
	}

	default OffsetDateTime getOffsetTimestamp(LocalDateTime localDateTime) {
		return localDateTime == null ? null
				: localDateTime.atZone(DATABASE_ZONE).withZoneSameInstant(OPENAPI_ZONE).toOffsetDateTime();
	}

	default LocalDateTime getFormattedLocalTimestamp() {
		return LocalDateTime.parse(DATABASE_FORMATTER.format(LocalDateTime.now(DATABASE_ZONE)));
	}

	default OffsetDateTime getFormattedOffsetTimestamp() {
		return OffsetDateTime.parse(OPENAPI_FORMATTER.format(LocalDateTime.now(DATABASE_ZONE).atZone(DATABASE_ZONE)
				.withZoneSameInstant(OPENAPI_ZONE).toOffsetDateTime()));
	}

	default LocalDateTime getFormattedLocalTimestamp(OffsetDateTime offsetDateTime) {
		return offsetDateTime == null ? null
				: LocalDateTime.parse(DATABASE_FORMATTER
						.format(offsetDateTime.withOffsetSameInstant(DATABASE_ZONE).toLocalDateTime()));
	}

	default OffsetDateTime getFormattedOffsetTimestamp(LocalDateTime localDateTime) {
		return localDateTime == null ? null
				: OffsetDateTime.parse(OPENAPI_FORMATTER.format(
						localDateTime.atZone(DATABASE_ZONE).withZoneSameInstant(OPENAPI_ZONE).toOffsetDateTime()));
	}

}
