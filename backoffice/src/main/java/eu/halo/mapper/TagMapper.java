package eu.halo.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import eu.halo.model.TagEntity;
import eu.halo.openapi.openmodels.Tag;

@Mapper
public interface TagMapper extends APITimestamp {

	public static TagMapper INSTANCE = Mappers.getMapper(TagMapper.class);

	@Mapping(source = "id", target = "tagId")
	Tag mapEntityToApi(TagEntity tagEntity);

	@Mapping(source = "tagId", target = "id")
	TagEntity mapApiToEntity(Tag tag);

	List<Tag> mapEntityToApi(List<TagEntity> tagEntityList);

	List<TagEntity> mapApiToEntity(List<Tag> tagList);

}
