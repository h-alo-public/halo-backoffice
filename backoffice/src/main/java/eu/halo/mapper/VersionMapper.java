package eu.halo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import eu.halo.openapi.openmodels.VersionResponse;

@Mapper
public interface VersionMapper extends APITimestamp {

	public static VersionMapper INSTANCE = Mappers.getMapper(VersionMapper.class);

	@Mapping(target = "timestamp", expression = "java(getFormattedOffsetTimestamp())")
	VersionResponse mapVersionToResponse(String version);

}
