package eu.halo.mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import eu.halo.model.ResultEntity;
import eu.halo.model.TagEntity;
import eu.halo.openapi.openmodels.Result;
import eu.halo.openapi.openmodels.ResultMutable;

@Mapper
public interface ResultMapper extends APITimestamp {

	public static ResultMapper INSTANCE = Mappers.getMapper(ResultMapper.class);

	@Mapping(source = "id", target = "resultId")
	@Mapping(target = "resultTS", expression = "java(getOffsetTimestamp(resultEntity.getCreated()))")
	@Mapping(target = "result", expression = "java(getBigDecimal(resultEntity.getResult()))")
	Result mapEntityToApi(ResultEntity resultEntity);

	default BigDecimal getBigDecimal(Double value) {
		return value == null ? null : BigDecimal.valueOf(value);
	}

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "created", expression = "java(getFormattedLocalTimestamp(resultMutable.getResultTS()))")
	@Mapping(target = "result", expression = "java(getDouble(resultMutable.getResult()))")
	@Mapping(target = "site", ignore = true)
	@Mapping(target = "tags", ignore = true)
	ResultEntity mapApiToEntity(ResultMutable resultMutable);

	default Double getDouble(BigDecimal value) {
		return value == null ? null : value.doubleValue();
	}

	List<Result> mapEntityToApi(List<ResultEntity> resultEntityList);

	default String mapEntityToString(TagEntity tagEntity) {
		return tagEntity.getTag();
	}

	List<String> mapEntityToString(Set<TagEntity> tagEntitySet);

}
