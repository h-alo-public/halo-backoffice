package eu.halo.mapper;

import java.math.BigDecimal;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import eu.halo.model.SiteEntity;
import eu.halo.openapi.openmodels.Site;

@Mapper
public interface SiteMapper extends APITimestamp {

	public static SiteMapper INSTANCE = Mappers.getMapper(SiteMapper.class);

	@Mapping(source = "site", target = "name")
	@Mapping(source = "id", target = "siteId")
	@Mapping(target = "createdTS", expression = "java(getOffsetTimestamp(siteEntity.getCreated()))")
	@Mapping(target = "updatedTS", expression = "java(getOffsetTimestamp(siteEntity.getUpdated()))")
	@Mapping(target = "siteStats.testsCount", expression = "java(getBigDecimal(siteEntity.getNum()))")
	@Mapping(target = "siteStats.minScore", expression = "java(getBigDecimal(siteEntity.getMin()))")
	@Mapping(target = "siteStats.maxScore", expression = "java(getBigDecimal(siteEntity.getMax()))")
	@Mapping(target = "siteStats.avgScore", expression = "java(getBigDecimal(siteEntity.getAvg()))")
	Site mapEntityToApi(SiteEntity siteEntity);

	default BigDecimal getBigDecimal(Long value) {
		return value == null ? null : BigDecimal.valueOf(value);
	}

	default BigDecimal getBigDecimal(Double value) {
		return value == null ? null : BigDecimal.valueOf(value);
	}

	List<Site> mapEntityToApi(List<SiteEntity> siteEntityList);

}
