package eu.halo.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties("spring.rabbitmq")
public class RabbitMQProperties {

	private String host = "localhost";

    private int port = 5672;

    private String username = "guest";
    
    private String password = "guest";

}
