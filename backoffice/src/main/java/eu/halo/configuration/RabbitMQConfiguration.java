package eu.halo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rabbitmq.client.ConnectionFactory;

@Configuration
@EnableConfigurationProperties(RabbitMQProperties.class)
public class RabbitMQConfiguration {

	@Autowired
	private RabbitMQProperties rabbitMQProperties;

	@Bean
	public ConnectionFactory connectionFactory() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(rabbitMQProperties.getHost());
		factory.setPort(rabbitMQProperties.getPort());
		factory.setUsername(rabbitMQProperties.getUsername());
		factory.setPassword(rabbitMQProperties.getPassword());
		return factory;
	}

}
