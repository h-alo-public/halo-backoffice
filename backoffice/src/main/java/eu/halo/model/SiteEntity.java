package eu.halo.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Table(name = "site")
@Getter
@Setter
public class SiteEntity {

	@Id
	@GeneratedValue(generator = "UUID")
	@Column(name = "id", updatable = false, nullable = false)
	private UUID id;

	@NonNull
	@Column(unique = true)
	private String site;

	@NonNull
	private LocalDateTime created;

	private LocalDateTime updated;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	@JsonIgnore
	@OneToMany(mappedBy = "site", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ResultEntity> results = new ArrayList<ResultEntity>();

	private Long num;

	private Double min;

	private Double max;

	private Double avg;

	public SiteEntity() {
		this.num = 0L;
	}

	@Override
	public int hashCode() {
		return Objects.hash(avg, created, id, max, min, num, site, updated);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SiteEntity other = (SiteEntity) obj;
		return Double.doubleToLongBits(avg) == Double.doubleToLongBits(other.avg)
				&& Objects.equals(created, other.created) && Objects.equals(id, other.id)
				&& Double.doubleToLongBits(max) == Double.doubleToLongBits(other.max)
				&& Double.doubleToLongBits(min) == Double.doubleToLongBits(other.min) && num == other.num
				&& Objects.equals(site, other.site) && Objects.equals(updated, other.updated);
	}

	@Override
	public String toString() {
		return "SiteEntity [id=" + id + ", site=" + site + ", created=" + created + ", updated=" + updated + ", num="
				+ num + ", min=" + min + ", max=" + max + ", avg=" + avg + "]";
	}

}
