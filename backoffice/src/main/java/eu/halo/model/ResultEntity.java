package eu.halo.model;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Table(name = "result")
@Getter
@Setter
public class ResultEntity {

	@Id
	@GeneratedValue(generator = "UUID")
	@Column(name = "id", updatable = false, nullable = false)
	private UUID id;

	@NonNull
	private LocalDateTime created;

	@NonNull
	private String contributor;

	@NonNull
	private String device;

	@NonNull
	private String cartridge;

	private String batch;

	@NonNull
	private Double result;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "site_id")
	private SiteEntity site;

	private String comment;

	@JsonIgnore
	@OrderBy("tag asc")
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "result_tag", joinColumns = { @JoinColumn(name = "result_id") }, inverseJoinColumns = {
			@JoinColumn(name = "tag_id") })
	private Set<TagEntity> tags = new LinkedHashSet<TagEntity>();

	@Override
	public int hashCode() {
		return Objects.hash(batch, cartridge, comment, contributor, created, device, id, result);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultEntity other = (ResultEntity) obj;
		return Objects.equals(batch, other.batch) && Objects.equals(cartridge, other.cartridge)
				&& Objects.equals(comment, other.comment) && Objects.equals(contributor, other.contributor)
				&& Objects.equals(created, other.created) && Objects.equals(device, other.device)
				&& Objects.equals(id, other.id) && Objects.equals(result, other.result);
	}

	@Override
	public String toString() {
		return "ResultEntity [id=" + id + ", created=" + created + ", contributor=" + contributor + ", device=" + device
				+ ", cartridge=" + cartridge + ", batch=" + batch + ", result=" + result + ", comment=" + comment + "]";
	}

}
