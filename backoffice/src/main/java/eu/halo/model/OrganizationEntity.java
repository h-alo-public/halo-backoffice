package eu.halo.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Table(name = "organization")
@Getter
@Setter
public class OrganizationEntity {

	@Id
	@GeneratedValue(generator = "UUID")
	@Column(name = "id", updatable = false, nullable = false)
	private UUID id;

	@NonNull
	@Column(unique = true)
	private String organization;

	@NonNull
	private LocalDateTime created;

	private LocalDateTime updated;

	@JsonIgnore
	@OneToMany(mappedBy = "organization", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<SiteEntity> sites = new ArrayList<SiteEntity>();

	@Override
	public int hashCode() {
		return Objects.hash(created, id, organization, updated);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationEntity other = (OrganizationEntity) obj;
		return Objects.equals(created, other.created) && Objects.equals(id, other.id)
				&& Objects.equals(organization, other.organization) && Objects.equals(updated, other.updated);
	}

	@Override
	public String toString() {
		return "OrganizationEntity [id=" + id + ", organization=" + organization + ", created=" + created + ", updated="
				+ updated + "]";
	}

}
