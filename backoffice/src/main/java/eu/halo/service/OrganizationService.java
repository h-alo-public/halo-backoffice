package eu.halo.service;

import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.halo.mapper.OrganizationMapper;
import eu.halo.model.OrganizationEntity;
import eu.halo.openapi.openmodels.OrganizationMutable;
import eu.halo.repository.OrganizationRepository;
import eu.halo.service.util.APIException;

@Service
@Transactional
public class OrganizationService {

	private final OrganizationRepository organizationRepository;

	private final UserInfoService userInfoService;

	private final TagService tagService;

	public OrganizationService(final OrganizationRepository organizationRepository,
			final UserInfoService userInfoService, final TagService tagService) {
		this.organizationRepository = organizationRepository;
		this.userInfoService = userInfoService;
		this.tagService = tagService;
	}

	public long getCount() {
		return organizationRepository.count();
	}

	public List<OrganizationEntity> getOrganizations() {
		return organizationRepository.findAll(Sort.by(Sort.Direction.ASC, "created"));
	}

	public List<OrganizationEntity> getOrganizations(int offset, int limit) {
		return organizationRepository.findAll(PageRequest.of(offset, limit, Sort.by(Sort.Direction.ASC, "created")))
				.getContent();
	}

	public OrganizationEntity getOrganization(String organizationId) {
		OrganizationEntity organizationEntity = organizationRepository.findById(UUID.fromString(organizationId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Organization not found!"));
		userInfoService.checkDataAccess(organizationEntity);
		return organizationEntity;
	}

	public OrganizationEntity createOrganization(OrganizationMutable organizationMutable) {
		OrganizationEntity organizationEntity = new OrganizationEntity();
		organizationEntity.setOrganization(organizationMutable.getName());
		organizationEntity.setCreated(OrganizationMapper.INSTANCE.getFormattedLocalTimestamp());
		return organizationRepository.save(organizationEntity);
	}

	public OrganizationEntity updateOrganization(String organizationId, OrganizationMutable organizationMutable) {
		OrganizationEntity organizationEntity = organizationRepository.findById(UUID.fromString(organizationId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Organization not found!"));
		organizationEntity.setOrganization(organizationMutable.getName());
		organizationEntity.setUpdated(OrganizationMapper.INSTANCE.getFormattedLocalTimestamp());
		organizationRepository.save(organizationEntity);
		return organizationEntity;
	}

	public OrganizationEntity deleteOrganization(String organizationId) {
		OrganizationEntity organizationEntity = organizationRepository.findById(UUID.fromString(organizationId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Organization not found!"));
		organizationRepository.deleteById(UUID.fromString(organizationId));
		tagService.deleteTags();
		return organizationEntity;
	}

}
