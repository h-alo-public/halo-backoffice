package eu.halo.service;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.halo.model.TagEntity;
import eu.halo.repository.TagRepository;

@Service
@Transactional
public class TagService {

	private final TagRepository tagRepository;

	public TagService(final TagRepository tagRepository) {
		this.tagRepository = tagRepository;
	}

	public List<TagEntity> getTags() {
		return tagRepository.findAll(Sort.by(Sort.Direction.ASC, "tag"));
	}

	public List<TagEntity> getTags(String organizationId) {
		return tagRepository.findTagsByOrganizationId(UUID.fromString(organizationId));
	}

	public List<TagEntity> getTags(Set<String> tagSet) {
		return tagRepository.findByTagIn(tagSet);
	}

	public void deleteTags() {
		tagRepository.deleteNotUsedTags();
	}

	public void deleteTags(List<UUID> uuidList) {
		tagRepository.deleteNotUsedTagsByTagIds(uuidList);
	}

}
