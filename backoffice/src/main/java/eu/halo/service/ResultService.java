package eu.halo.service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.halo.mapper.ResultMapper;
import eu.halo.model.ResultEntity;
import eu.halo.model.SiteEntity;
import eu.halo.model.TagEntity;
import eu.halo.openapi.openmodels.ResultMutable;
import eu.halo.repository.ResultRepository;
import eu.halo.service.util.APIException;

@Service
@Transactional
public class ResultService {
	
	private final String SORT_PARAMS_SEPARATOR = ":";

	private final ResultRepository resultRepository;

	private final SiteService siteService;

	private final UserInfoService userInfoService;

	private final TagService tagService;

	public ResultService(final ResultRepository resultRepository, final SiteService siteService,
			final UserInfoService userInfoService, final TagService tagService) {
		this.resultRepository = resultRepository;
		this.siteService = siteService;
		this.userInfoService = userInfoService;
		this.tagService = tagService;
	}

	public long getCount(String siteId) {
		siteService.getSite(siteId);
		return resultRepository.countBySiteId(UUID.fromString(siteId));
	}

	public List<ResultEntity> getResults(String siteId) {
		siteService.getSite(siteId);
		return resultRepository.findBySiteId(UUID.fromString(siteId), Sort.by(Sort.Direction.DESC, "created"));
	}

	public List<ResultEntity> getResults(String siteId, int offset, int limit, List<String> sort) {
		siteService.getSite(siteId);
		return sort == null
				? resultRepository.findBySiteId(UUID.fromString(siteId),
						PageRequest.of(offset, limit, Sort.by(Sort.Direction.DESC, "created")))
				: resultRepository.findBySiteId(UUID.fromString(siteId), PageRequest.of(offset, limit,
						Sort.by(getSortOrderList(sort)).and(Sort.by(Sort.Direction.DESC, "created"))));
	}

	public List<ResultEntity> getResults(String siteId, int offset, int limit, String tag, List<String> sort) {
		siteService.getSite(siteId);
		return sort == null
				? resultRepository.findBySiteIdAndTagName(UUID.fromString(siteId), tag,
						PageRequest.of(offset, limit, Sort.by(Sort.Direction.DESC, "created")))
				: resultRepository.findBySiteIdAndTagName(UUID.fromString(siteId), tag, PageRequest.of(offset, limit,
						Sort.by(getSortOrderList(sort)).and(Sort.by(Sort.Direction.DESC, "created"))));
	}

	private List<Sort.Order> getSortOrderList(List<String> sort) {
		if (sort == null) {
			return null;
		}
		if (sort.size() == 0) {
			throw new APIException(HttpStatus.BAD_REQUEST, "Sort parameters wrong format!");
		}
		List<Sort.Order> order = new ArrayList<>();
		for (String str : sort) {
			String[] params = str.split(SORT_PARAMS_SEPARATOR);
			if (params.length == 1) {
				order.add(new Sort.Order(Sort.Direction.ASC, params[0]));
			} else if (params.length == 2) {
				if (params[1].equalsIgnoreCase("ASC")) {
					order.add(new Sort.Order(Sort.Direction.ASC, params[0]));
				} else if (params[1].equalsIgnoreCase("DESC")) {
					order.add(new Sort.Order(Sort.Direction.DESC, params[0]));
				} else {
					throw new APIException(HttpStatus.BAD_REQUEST, "Sort parameters wrong format!");
				}
			} else {
				throw new APIException(HttpStatus.BAD_REQUEST, "Sort parameters wrong format!");
			}
		}
		return order;
	}

	public ResultEntity getResult(String resultId) {
		ResultEntity resultEntity = resultRepository.findById(UUID.fromString(resultId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Result not found!"));
		userInfoService.checkDataAccess(resultEntity);
		return resultEntity;
	}

	public ResultEntity createResult(String siteId, ResultMutable resultMutable) {
		SiteEntity siteEntity = siteService.getSite(siteId);
		userInfoService.checkDataAccess(siteEntity);
		ResultEntity resultEntity = ResultMapper.INSTANCE.mapApiToEntity(resultMutable);
		createTags(resultEntity, resultMutable);
		resultEntity.setSite(siteEntity);
		resultEntity = resultRepository.save(resultEntity);
		siteService.updateSiteStats(siteId);
		return resultEntity;
	}

	private void createTags(ResultEntity resultEntity, ResultMutable resultMutable) {
		Set<TagEntity> tagEntitySet = resultEntity.getTags();
		List<String> requestTags = resultMutable.getTags();

		if (requestTags == null) {
			return;
		}

		Set<String> addTags = mapStringListToStringSet(requestTags);
		List<TagEntity> tagEntityList = tagService.getTags(addTags);

		for (TagEntity tagEntity : tagEntityList) {
			String tag = tagEntity.getTag();
			if (requestTags.contains(tag)) {
				tagEntitySet.add(tagEntity);
				addTags.remove(tag);
			}
		}

		for (String tag : addTags) {
			tagEntitySet.add(new TagEntity(tag));
		}
	}

	public ResultEntity updateResult(String resultId, ResultMutable resultMutable) {
		ResultEntity resultEntity = resultRepository.findById(UUID.fromString(resultId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Result not found!"));
		userInfoService.checkDataAccess(resultEntity);
		resultEntity.setCreated(ResultMapper.INSTANCE.getFormattedLocalTimestamp(resultMutable.getResultTS()));
		resultEntity.setContributor(resultMutable.getContributor());
		resultEntity.setDevice(resultMutable.getDevice());
		resultEntity.setCartridge(resultMutable.getCartridge());
		resultEntity.setBatch(resultMutable.getBatch());
		resultEntity.setResult(ResultMapper.INSTANCE.getDouble(resultMutable.getResult()));
		resultEntity.setComment(resultMutable.getComment());
		List<UUID> tagIds = updateTags(resultEntity, resultMutable);
		resultEntity = resultRepository.saveAndFlush(resultEntity);
		tagService.deleteTags(tagIds);
		siteService.updateSiteStats(resultEntity.getSite().getId().toString());
		return resultEntity;
	}

	private List<UUID> updateTags(ResultEntity resultEntity, ResultMutable resultMutable) {
		Set<TagEntity> tagEntitySet = resultEntity.getTags();
		List<String> requestTags = resultMutable.getTags();

		if (requestTags == null) {
			return null;
		}

		Set<TagEntity> deleteTags = new LinkedHashSet<TagEntity>(tagEntitySet);
		Set<String> addTags = mapStringListToStringSet(requestTags);
		List<TagEntity> tagEntityList = tagService.getTags(addTags);

		for (TagEntity tagEntity : tagEntitySet) {
			String tag = tagEntity.getTag();
			if (requestTags.contains(tag)) {
				deleteTags.remove(tagEntity);
				addTags.remove(tag);
			}
		}

		for (TagEntity tagEntity : tagEntityList) {
			String tag = tagEntity.getTag();
			if (requestTags.contains(tag)) {
				tagEntitySet.add(tagEntity);
				addTags.remove(tag);
			}
		}

		for (String tag : addTags) {
			tagEntitySet.add(new TagEntity(tag));
		}

		tagEntitySet.removeAll(deleteTags);

		List<UUID> tagIds = new ArrayList<UUID>(deleteTags.size());
		for (TagEntity tagEntity : deleteTags) {
			tagIds.add(tagEntity.getId());
		}

		return tagIds;
	}

	public ResultEntity deleteResult(String resultId) {
		ResultEntity resultEntity = resultRepository.findById(UUID.fromString(resultId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Result not found"));
		userInfoService.checkDataAccess(resultEntity);
		List<UUID> tagIds = deleteTags(resultEntity);
		resultRepository.deleteById(UUID.fromString(resultId));
		tagService.deleteTags(tagIds);
		siteService.updateSiteStats(resultEntity.getSite().getId().toString());
		return resultEntity;
	}

	private List<UUID> deleteTags(ResultEntity resultEntity) {
		Set<TagEntity> tagEntitySet = resultEntity.getTags();
		List<UUID> tagIds = new ArrayList<UUID>(tagEntitySet.size());
		for (TagEntity tagEntity : tagEntitySet) {
			tagIds.add(tagEntity.getId());
		}

		return tagIds;
	}

	private Set<String> mapStringListToStringSet(List<String> stringList) {
		if (stringList == null) {
			return null;
		}

		Set<String> stringSet = new LinkedHashSet<String>(Math.max((int) (stringList.size() / .75f) + 1, 16));
		for (String string : stringList) {
			if (string != null && string.length() != 0) {
				stringSet.add(string);
			}
		}

		return stringSet;
	}

}
