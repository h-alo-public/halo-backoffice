package eu.halo.service;

import java.util.UUID;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import eu.halo.model.OrganizationEntity;
import eu.halo.model.ResultEntity;
import eu.halo.model.SiteEntity;
import eu.halo.service.util.APIException;
import eu.halo.service.util.UserData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserInfoService {

	public static final String ADMIN_ROLE = "halo-admin";
	public static final String USER_ROLE = "halo-user";
	public static final String ORGANIZATION_ID = "organizationId";

	public UserData getUserData() {

		KeycloakSecurityContext keycloakContext = getKeycloakContext();

		if (keycloakContext == null) {
			throw new APIException(HttpStatus.BAD_REQUEST, "Unauthorized user!");
		}

		AccessToken accessToken = keycloakContext.getToken();

		UserData userData = new UserData();
		userData.setId(UUID.fromString(accessToken.getSubject()));
		userData.setName(accessToken.getPreferredUsername());
		userData.setAdminFlag(accessToken.getRealmAccess().isUserInRole(ADMIN_ROLE));

		if (!userData.isAdminFlag()) {
			try {
				userData.setOrganizationId(UUID.fromString((String) accessToken.getOtherClaims().get(ORGANIZATION_ID)));
			} catch (NullPointerException | IllegalArgumentException exception) {
				throw new APIException(HttpStatus.BAD_REQUEST, "Invalid organization id!");
			}
		}

		return userData;
	}

	public void checkDataAccess(OrganizationEntity organizationEntity) {
		UserData userData = getUserData();
		if (!userData.isAdminFlag() && !userData.getOrganizationId().equals(organizationEntity.getId())) {
			log.info(userData.toString());
			throw new APIException(HttpStatus.BAD_REQUEST, "Organization access is denied!");
		}
	}

	public void checkDataAccess(SiteEntity siteEntity) {
		UserData userData = getUserData();
		if (!userData.isAdminFlag() && !userData.getOrganizationId().equals(siteEntity.getOrganization().getId())) {
			log.info(userData.toString());
			throw new APIException(HttpStatus.BAD_REQUEST, "Site access is denied!");
		}
	}

	public void checkDataAccess(ResultEntity resultEntity) {
		UserData userData = getUserData();
		if (!userData.isAdminFlag()
				&& !userData.getOrganizationId().equals(resultEntity.getSite().getOrganization().getId())) {
			log.info(userData.toString());
			throw new APIException(HttpStatus.BAD_REQUEST, "Result access is denied!");
		}
	}

	private KeycloakSecurityContext getKeycloakContext() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal == null || !(principal instanceof KeycloakPrincipal)) {
			return null;
		}
		return ((KeycloakPrincipal<?>) principal).getKeycloakSecurityContext();
	}

}
