package eu.halo.service.util;

import java.util.Objects;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserData {

	private UUID id;

	private String name;

	private boolean adminFlag;

	private UUID organizationId;

	@Override
	public int hashCode() {
		return Objects.hash(adminFlag, id, name, organizationId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserData other = (UserData) obj;
		return adminFlag == other.adminFlag && Objects.equals(id, other.id) && Objects.equals(name, other.name)
				&& Objects.equals(organizationId, other.organizationId);
	}

	@Override
	public String toString() {
		return "UserData [id=" + id + ", name=" + name + ", adminFlag=" + adminFlag + ", organizationId="
				+ organizationId + "]";
	}

}
