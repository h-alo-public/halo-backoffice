package eu.halo.service.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ParametersChecker {

	public static final int OFFSET_MIN = 0;
	public static final int OFFSET_MAX = Integer.MAX_VALUE;
	public static final int LIMIT_MIN = 1;
	public static final int LIMIT_MAX = Integer.MAX_VALUE;

	private final int DEFAULT_FETCH_LIMIT = 20;
	public static int MAX_FETCH_LIMIT;

	@Value("${halo.api.maxfetchlimit:" + DEFAULT_FETCH_LIMIT + "}")
	public void setMaxFetchLimit(String value) {
		try {
			MAX_FETCH_LIMIT = Integer.parseInt(value);
		} catch (NumberFormatException e) {
			log.info("NumberFormatException: halo.api.maxfetchlimit (default: " + DEFAULT_FETCH_LIMIT + ")");
			MAX_FETCH_LIMIT = DEFAULT_FETCH_LIMIT;
		}
	}

	public static boolean checkOffsetAndLimit(Integer offset, Integer limit) {
		if (offset == null && limit == null) {
			return false;
		} else if (offset == null || limit == null) {
			throw new APIException(HttpStatus.BAD_REQUEST, "One of the paging parameters is missing!");
		} else if (offset < OFFSET_MIN || limit < LIMIT_MIN || offset > OFFSET_MAX || limit > LIMIT_MAX) {
			throw new APIException(HttpStatus.BAD_REQUEST,
					new StringBuilder(128).append("Paging parameters are out of range! (").append(OFFSET_MIN)
							.append(" <= offset <= ").append(OFFSET_MAX).append(", ").append(LIMIT_MIN)
							.append(" <= limit <= ").append(LIMIT_MAX).append(")").toString());
		} else if ((long) offset * limit > Integer.MAX_VALUE) {
			throw new APIException(HttpStatus.BAD_REQUEST,
					new StringBuilder(128).append("Product of paging parameters (offset * limit = ")
							.append((long) offset * limit).append(") is out of range! (").append(OFFSET_MIN * LIMIT_MIN)
							.append(" <= offset * limit <= ").append(Integer.MAX_VALUE).append(")").toString());
		} else {
			return true;
		}
	}

	public static boolean checkTag(String tag) {
		if (tag == null) {
			return false;
		} else if (tag.equals("")) {
			throw new APIException(HttpStatus.BAD_REQUEST, "Tag should not be empty!");
		} else {
			return true;
		}
	}

}
