package eu.halo.service.util;

import org.springframework.http.HttpStatus;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class APIException extends RuntimeException {

	private HttpStatus status;

	private String message;

	public APIException(HttpStatus status, String message) {
		this.status = status;
		this.message = message;
	}

	@Override
	public String toString() {
		return "ExceptionEntity [status=" + status + ", message=" + message + "]";
	}

}
