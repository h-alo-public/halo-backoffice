package eu.halo.service;

import java.io.InputStream;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.halo.service.util.APIException;
import io.minio.BucketExistsArgs;
import io.minio.ListObjectsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveBucketArgs;
import io.minio.RemoveObjectArgs;
import io.minio.Result;
import io.minio.UploadObjectArgs;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MinioService {

	private final MinioClient minioClient;

	@Autowired
	public MinioService(MinioClient minioClient) {
		this.minioClient = minioClient;
	}

	public void uploadObject(String bucketName, String objectName, String filePath) {
		createBucket(bucketName);
		try {
			minioClient.uploadObject(
					UploadObjectArgs.builder().bucket(bucketName).object(objectName).filename(filePath).build());
		} catch (Exception e) {
			log.error("Minio uploadObject error", e);
			throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	public void uploadStream(String bucketName, String objectName, InputStream inputStream) {
		createBucket(bucketName);
		try {
			minioClient.putObject(PutObjectArgs.builder().bucket(bucketName).object(objectName)
					.stream(inputStream, inputStream.available(), -1).build());
		} catch (Exception e) {
			log.error("Minio uploadStream error", e);
			throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	public List<Bucket> listBuckets() {
		try {
			return minioClient.listBuckets();
		} catch (Exception e) {
			log.error("Minio listBuckets error", e);
			throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	public Iterable<Result<Item>> listObjects(String bucketName) {
		return bucketExists(bucketName)
				? minioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName).recursive(true).build())
				: null;
	}

	public boolean bucketExists(String bucketName) {
		try {
			return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
		} catch (Exception e) {
			log.error("Minio bucketExists("+bucketName+") error", e);
			throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	public boolean objectExists(String bucketName, String objectName) {
		try {
			Iterable<Result<Item>> bucketObjects = listObjects(bucketName);
			for (Result<Item> result : bucketObjects) {
				if (result.get().objectName().equals(objectName)) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error("Minio objectExists error", e);
			throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return false;
	}

	public void createBucket(String bucketName) {
		if (!bucketExists(bucketName)) {
			try {
				minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
				log.info("Bucket " + bucketName + " is created successfully");
			} catch (Exception e) {
				log.error("Minio createBucket error", e);
				throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
			}
		}
	}

	public void removeBucket(String bucketName) {
		if (bucketExists(bucketName)) {
			try {
				minioClient.removeBucket(RemoveBucketArgs.builder().bucket(bucketName).build());
				log.info("Bucket " + bucketName + " is removed successfully");
			} catch (Exception e) {
				log.error("Minio removeBucket error", e);
				throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
			}
		} else {
			log.info("Bucket " + bucketName + " does not exist");
		}
	}

	public void removeObject(String bucketName, String objectName) {
		if (bucketExists(bucketName)) {
			try {
				if (objectExists(bucketName, objectName)) {
					minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
					log.info("Object " + objectName + " is removed successfully");
				} else {
					log.info("Object " + objectName + " does not exist");
				}
			} catch (Exception e) {
				log.error("Minio removeObject error", e);
				throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
			}
		} else {
			log.info("Bucket " + bucketName + " does not exist");
		}
	}

}
