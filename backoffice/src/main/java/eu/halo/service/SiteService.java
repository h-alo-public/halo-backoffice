package eu.halo.service;

import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.halo.mapper.SiteMapper;
import eu.halo.model.OrganizationEntity;
import eu.halo.model.SiteEntity;
import eu.halo.openapi.openmodels.SiteMutable;
import eu.halo.repository.SiteRepository;
import eu.halo.service.util.APIException;

@Service
@Transactional
public class SiteService {

	private final SiteRepository siteRepository;

	private final OrganizationService organizationService;

	private final UserInfoService userInfoService;

	private final TagService tagService;

	public SiteService(final SiteRepository siteRepository, final OrganizationService organizationService,
			final UserInfoService userInfoService, final TagService tagService) {
		this.siteRepository = siteRepository;
		this.organizationService = organizationService;
		this.userInfoService = userInfoService;
		this.tagService = tagService;
	}

	public long getCount(String organizationId) {
		organizationService.getOrganization(organizationId);
		return siteRepository.countByOrganizationId(UUID.fromString(organizationId));
	}

	public List<SiteEntity> getSites(String organizationId) {
		organizationService.getOrganization(organizationId);
		return siteRepository.findByOrganizationId(UUID.fromString(organizationId),
				Sort.by(Sort.Direction.ASC, "created"));
	}

	public List<SiteEntity> getSites(String organizationId, int offset, int limit) {
		organizationService.getOrganization(organizationId);
		return siteRepository.findByOrganizationId(UUID.fromString(organizationId),
				PageRequest.of(offset, limit, Sort.by(Sort.Direction.ASC, "created")));
	}

	public SiteEntity getSite(String siteId) {
		SiteEntity siteEntity = siteRepository.findById(UUID.fromString(siteId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Site not found!"));
		userInfoService.checkDataAccess(siteEntity);
		return siteEntity;
	}

	public SiteEntity getSiteByName(String siteName) {
		SiteEntity siteEntity = siteRepository.getBySite(siteName);
		if (siteEntity == null) {
			throw new APIException(HttpStatus.BAD_REQUEST, "Site not found!");
		}
		userInfoService.checkDataAccess(siteEntity);
		return siteEntity;
	}

	public SiteEntity createSite(String organizationId, SiteMutable siteMutable) {
		OrganizationEntity organizationEntity = organizationService.getOrganization(organizationId);
		SiteEntity siteEntity = new SiteEntity();
		siteEntity.setSite(siteMutable.getName());
		siteEntity.setOrganization(organizationEntity);
		siteEntity.setCreated(SiteMapper.INSTANCE.getFormattedLocalTimestamp());
		return siteRepository.save(siteEntity);
	}

	public SiteEntity updateSite(String siteId, SiteMutable siteMutable) {
		SiteEntity siteEntity = siteRepository.findById(UUID.fromString(siteId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Site not found!"));
		siteEntity.setSite(siteMutable.getName());
		siteEntity.setUpdated(SiteMapper.INSTANCE.getFormattedLocalTimestamp());
		siteRepository.save(siteEntity);
		return siteEntity;
	}

	public void updateSiteStats(String siteId) {
		siteRepository.updateSiteStatsBySiteId(UUID.fromString(siteId));
	}

	public SiteEntity deleteSite(String siteId) {
		SiteEntity siteEntity = siteRepository.findById(UUID.fromString(siteId))
				.orElseThrow(() -> new APIException(HttpStatus.BAD_REQUEST, "Site not found!"));
		siteRepository.deleteById(UUID.fromString(siteId));
		tagService.deleteTags();
		return siteEntity;
	}

}
