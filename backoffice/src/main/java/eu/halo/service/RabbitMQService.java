package eu.halo.service;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.UUID;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Slf4j
@Service
public class RabbitMQService {

	private final String QUEUE_NAME = "celery";

	ConnectionFactory connectionFactory;

	@Autowired
	RabbitMQService(ConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	public void sendTask(String taskName, String measurementId, String senderName, String siteId, String deviceId,
			String cartridgeId) {

		try (Connection connection = connectionFactory.newConnection(); Channel channel = connection.createChannel()) {

			channel.queueDeclare(QUEUE_NAME, true, false, false, null);

			JSONObject rootObject = getOrderedJSONObject();
			rootObject.put("id", UUID.randomUUID());
			rootObject.put("task", taskName);

			JSONObject kwargsObject = getOrderedJSONObject();
			kwargsObject.put("measurement_id", measurementId);
			kwargsObject.put("sender_name", senderName);
			kwargsObject.put("site_uuid", siteId);
			kwargsObject.put("device_id", deviceId);
			kwargsObject.put("cartridge_id", cartridgeId);
			rootObject.put("kwargs", kwargsObject);

			channel.basicPublish("", QUEUE_NAME,
					new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("utf-8").build(),
					rootObject.toString().getBytes("utf-8"));

			log.info("Sent message:\n" + rootObject.toString(4));

		} catch (Exception e) {
			log.info(e.getMessage());
		}
	}

	private JSONObject getOrderedJSONObject() {
		JSONObject jsonObject = new JSONObject();
		try {
			Field changeMap = jsonObject.getClass().getDeclaredField("map");
			changeMap.setAccessible(true);
			changeMap.set(jsonObject, new LinkedHashMap<>());
			changeMap.setAccessible(false);
		} catch (IllegalAccessException | NoSuchFieldException e) {
			log.info(e.getMessage());
		}
		return jsonObject;
	}

}
