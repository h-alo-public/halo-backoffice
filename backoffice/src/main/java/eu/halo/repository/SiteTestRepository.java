package eu.halo.repository;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Repository
@Profile({ "test" })
public interface SiteTestRepository extends SiteRepository {

	@Modifying
	@Transactional
	@Query(value = "UPDATE site s SET "
			+ "num=coalesce((SELECT count(*) AS num FROM result r WHERE r.site_id=s.id GROUP BY site_id),0), "
			+ "min=(SELECT min(result) AS min FROM result r WHERE r.site_id=s.id GROUP BY site_id), "
			+ "max=(SELECT max(result) AS max FROM result r WHERE r.site_id=s.id GROUP BY site_id), "
			+ "avg=(SELECT round(avg(result),2) AS avg FROM result r WHERE r.site_id=s.id GROUP BY site_id)", nativeQuery = true)
	void updateSiteStats();

	@Modifying
	@Transactional
	@Query(value = "UPDATE site s SET "
			+ "num=coalesce((SELECT count(*) AS num FROM result r WHERE r.site_id=s.id GROUP BY site_id),0), "
			+ "min=(SELECT min(result) AS min FROM result r WHERE r.site_id=s.id GROUP BY site_id), "
			+ "max=(SELECT max(result) AS max FROM result r WHERE r.site_id=s.id GROUP BY site_id), "
			+ "avg=(SELECT round(avg(result),2) AS avg FROM result r WHERE r.site_id=s.id GROUP BY site_id) "
			+ "WHERE s.id=:siteId", nativeQuery = true)
	void updateSiteStatsBySiteId(@Param("siteId") UUID siteId);

}
