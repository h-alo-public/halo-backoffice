package eu.halo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import eu.halo.model.TagEntity;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface TagRepository extends JpaRepository<TagEntity, UUID> {

	List<TagEntity> findByTagIn(Set<String> tagSet);

	@Query(value = "SELECT t.id, t.tag FROM site AS s JOIN result AS r ON s.id=r.site_id JOIN result_tag AS rt ON r.id=rt.result_id JOIN tag AS t ON rt.tag_id=t.id "
			+ "WHERE s.organization_id=:organizationId GROUP BY t.id, t.tag ORDER BY t.tag", nativeQuery = true)
	List<TagEntity> findTagsByOrganizationId(@Param("organizationId") UUID organizationId);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM tag WHERE id IN "
			+ "(SELECT t.id FROM result_tag rt RIGHT JOIN tag t ON rt.tag_id = t.id GROUP BY t.id HAVING count(rt.tag_id) = 0)", nativeQuery = true)
	void deleteNotUsedTags();

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM tag WHERE id IN "
			+ "(SELECT t.id FROM result_tag rt RIGHT JOIN tag t ON rt.tag_id = t.id WHERE t.id IN (:tagIds) GROUP BY t.id HAVING count(rt.tag_id) = 0)", nativeQuery = true)
	void deleteNotUsedTagsByTagIds(@Param("tagIds") List<UUID> tagIds);

}
