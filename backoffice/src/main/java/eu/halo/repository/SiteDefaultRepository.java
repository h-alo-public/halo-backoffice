package eu.halo.repository;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Repository
@Profile({ "dev", "prod" })
public interface SiteDefaultRepository extends SiteRepository {

	@Modifying
	@Transactional
	@Query(value = "UPDATE site s1 SET num=coalesce(stat.num,0), min=stat.min, max=stat.max, avg=round(cast(stat.avg as numeric),2) "
			+ "FROM site s2 LEFT JOIN (SELECT site_id AS id, count(*) AS num, min(result) AS min, max(result) AS max, avg(result) AS avg "
			+ "FROM result GROUP BY site_id) AS stat ON stat.id=s2.id WHERE s1.id=s2.id", nativeQuery = true)
	void updateSiteStats();

	@Modifying
	@Transactional
	@Query(value = "UPDATE site s1 SET num=coalesce(stat.num,0), min=stat.min, max=stat.max, avg=round(cast(stat.avg as numeric),2) "
			+ "FROM site s2 LEFT JOIN (SELECT site_id AS id, count(*) AS num, min(result) AS min, max(result) AS max, avg(result) AS avg "
			+ "FROM result WHERE site_id=:siteId GROUP BY site_id) AS stat ON stat.id=s2.id WHERE s1.id=s2.id AND s1.id=:siteId", nativeQuery = true)
	void updateSiteStatsBySiteId(@Param("siteId") UUID siteId);

}
