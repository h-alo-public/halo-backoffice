package eu.halo.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import eu.halo.model.ResultEntity;
import java.util.List;
import java.util.UUID;

@Repository
public interface ResultRepository extends JpaRepository<ResultEntity, UUID> {

	long countBySiteId(UUID siteId);

	List<ResultEntity> findBySiteId(UUID siteId, Sort sort);

	List<ResultEntity> findBySiteId(UUID siteId, Pageable page);

	@Query(value = "SELECT r.id, r.created, r.contributor, r.device, r.cartridge, r.batch, r.result, r.comment, r.site_id "
			+ "FROM result AS r JOIN result_tag AS rt ON r.id=rt.result_id JOIN tag AS t ON rt.tag_id=t.id WHERE r.site_id=:siteId AND t.tag=:tagName", nativeQuery = true)
	List<ResultEntity> findBySiteIdAndTagName(@Param("siteId") UUID siteId, @Param("tagName") String tagName,
			Pageable page);

}
