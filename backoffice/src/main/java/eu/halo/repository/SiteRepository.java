package eu.halo.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import eu.halo.model.SiteEntity;
import java.util.List;
import java.util.UUID;

@NoRepositoryBean
public interface SiteRepository extends JpaRepository<SiteEntity, UUID> {

	long countByOrganizationId(UUID organizationId);

	List<SiteEntity> findByOrganizationId(UUID organizationId, Sort sort);

	List<SiteEntity> findByOrganizationId(UUID organizationId, Pageable page);

	void updateSiteStats();

	void updateSiteStatsBySiteId(UUID siteId);
	
	SiteEntity getBySite(String site);
	
}
