package eu.halo.controller;

import javax.annotation.security.RolesAllowed;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import eu.halo.mapper.UserInfoMapper;
import eu.halo.openapi.UserinfoApi;
import eu.halo.openapi.openmodels.UserInfoResponse;
import eu.halo.service.UserInfoService;
import eu.halo.service.util.UserData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
public class UserInfoController implements UserinfoApi {

	private final UserInfoService userInfoService;

	public UserInfoController(final UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<UserInfoResponse> userinfoGet() {
		UserData userData = userInfoService.getUserData();
		log.info(userData.toString());
		return new ResponseEntity<UserInfoResponse>(UserInfoMapper.INSTANCE.mapEntityToResponse(userData),
				HttpStatus.OK);
	}

}
