package eu.halo.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import eu.halo.mapper.TagMapper;
import eu.halo.openapi.TagsApi;
import eu.halo.openapi.openmodels.TagsResponse;
import eu.halo.service.TagService;
import eu.halo.service.UserInfoService;

@RestController
@RequestMapping("/api")
public class TagsController implements TagsApi {

	private final TagService tagService;

	public TagsController(final TagService tagService) {
		this.tagService = tagService;
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<TagsResponse> tagsGet() {
		TagsResponse tagsResponse = new TagsResponse();
		tagsResponse.setTags(TagMapper.INSTANCE.mapEntityToApi(tagService.getTags()));
		tagsResponse.setTimestamp(TagMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<TagsResponse>(tagsResponse, HttpStatus.OK);
	}

}
