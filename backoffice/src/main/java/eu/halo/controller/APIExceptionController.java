package eu.halo.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import eu.halo.mapper.APIExceptionMapper;
import eu.halo.openapi.openmodels.APIExceptionResponse;
import eu.halo.service.util.APIException;
import eu.halo.service.util.ParametersChecker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Slf4j
@RestControllerAdvice
public class APIExceptionController {

	@ExceptionHandler(APIException.class)
	public ResponseEntity<APIExceptionResponse> exceptionHandler(APIException exception) {
		log.info(exception.getMessage());
		return new ResponseEntity<APIExceptionResponse>(APIExceptionMapper.INSTANCE.mapEntityToResponse(exception),
				exception.getStatus());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<APIExceptionResponse> exceptionHandler(Exception exception) {
		log.info(new Object() {
		}.getClass().getEnclosingMethod().toString());
		if (exception instanceof NullPointerException) {
			String firstUp = getFirstUpMessage(exception.getMessage());
			if (firstUp == null) {
				firstUp = "Null value";
			}
			log.info(firstUp);
			return new ResponseEntity<APIExceptionResponse>(
					APIExceptionMapper.INSTANCE.mapEntityToResponse(new APIException(HttpStatus.BAD_REQUEST, firstUp)),
					HttpStatus.BAD_REQUEST);
		} else if (exception instanceof MethodArgumentTypeMismatchException) {
			log.info("Paging parameters are out of range!");
			return new ResponseEntity<APIExceptionResponse>(
					APIExceptionMapper.INSTANCE.mapEntityToResponse(new APIException(HttpStatus.BAD_REQUEST,
							new StringBuilder(128).append("Paging parameters are out of range! (")
									.append(ParametersChecker.OFFSET_MIN).append(" <= offset <= ")
									.append(ParametersChecker.OFFSET_MAX).append(", ")
									.append(ParametersChecker.LIMIT_MIN).append(" <= limit <= ")
									.append(ParametersChecker.LIMIT_MAX).append(")").toString())),
					HttpStatus.BAD_REQUEST);
		} else {
			log.info(exception.getMessage());
			return new ResponseEntity<APIExceptionResponse>(APIExceptionMapper.INSTANCE.mapEntityToResponse(exception),
					HttpStatus.BAD_REQUEST);
		}
	}

	private String getFirstUpMessage(String message) {
		return message == null ? null : message.substring(0, 1).toUpperCase() + message.substring(1);
	}

}
