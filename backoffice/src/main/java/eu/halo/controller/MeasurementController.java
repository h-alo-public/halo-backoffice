package eu.halo.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import eu.halo.mapper.MeasurementInfoMapper;
import eu.halo.openapi.MeasurementApi;
import eu.halo.openapi.openmodels.MeasurementResponse;
import eu.halo.service.MinioService;
import eu.halo.service.RabbitMQService;
import eu.halo.service.SiteService;
import eu.halo.service.UserInfoService;
import eu.halo.service.util.APIException;

@RestController
@RequestMapping("/api")
public class MeasurementController implements MeasurementApi {

	private final SiteService siteService;

	private final UserInfoService userInfoService;

	private final MinioService minioService;

	private final RabbitMQService rabbitMQService;

	public MeasurementController(final SiteService siteService, final UserInfoService userInfoService,
			final MinioService minioService, final RabbitMQService rabbitMQService) {
		this.siteService = siteService;
		this.userInfoService = userInfoService;
		this.minioService = minioService;
		this.rabbitMQService = rabbitMQService;
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<MeasurementResponse> measurementPost(@Valid String sitename, @Valid String device,
			@Valid String cartridge, MultipartFile filedata) {

		int index;
		String fileName = filedata.getOriginalFilename();
		String fileExtension = (index = fileName.lastIndexOf('.')) > 0 ? fileName.substring(index + 1).toLowerCase()
				: "";

		if (!(fileExtension.equals("csv") || fileExtension.equals("json"))) {
			throw new APIException(HttpStatus.BAD_REQUEST, "Supported file types: csv, json!");
		}

		String fileTimestamp = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss-SSS")
				.format(LocalDateTime.now(ZoneOffset.UTC));
		String objectName = fileTimestamp + "." + fileExtension;
		String siteId;

		try {
			siteId = siteService.getSiteByName(sitename).getId().toString().toLowerCase();
			minioService.uploadStream(siteId, objectName, filedata.getInputStream());
		} catch (IOException e) {
			throw new APIException(HttpStatus.BAD_REQUEST, e.getMessage());
		}

		if (fileExtension.equals("csv")) {
			rabbitMQService.sendTask("process_csv", objectName, userInfoService.getUserData().getName(), siteId, device,
					cartridge);
		} else if (fileExtension.equals("json")) {
			rabbitMQService.sendTask("process_json", objectName, userInfoService.getUserData().getName(), siteId,
					device, cartridge);
		}

		MeasurementResponse measurementResponse = new MeasurementResponse();
		measurementResponse.setMeasurementInfo(MeasurementInfoMapper.INSTANCE.mapStringsToMeasurementInfo(sitename,
				device, cartridge, filedata.getOriginalFilename()));
		measurementResponse.setTimestamp(MeasurementInfoMapper.INSTANCE.getOffsetTimestamp());

		return new ResponseEntity<MeasurementResponse>(measurementResponse, HttpStatus.OK);
	}

}
