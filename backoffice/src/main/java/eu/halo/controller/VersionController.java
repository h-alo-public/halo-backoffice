package eu.halo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import eu.halo.mapper.VersionMapper;
import eu.halo.openapi.VersionApi;
import eu.halo.openapi.openmodels.VersionResponse;

@RestController
@RequestMapping("/api")
public class VersionController implements VersionApi {

	@Value("${halo.api.version:undefined}")
	private String apiVersion;

	@Override
	public ResponseEntity<VersionResponse> versionGet() {
		return new ResponseEntity<VersionResponse>(VersionMapper.INSTANCE.mapVersionToResponse(apiVersion),
				HttpStatus.OK);
	}

}
