package eu.halo.controller;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import eu.halo.mapper.ResultMapper;
import eu.halo.openapi.ResultsApi;
import eu.halo.openapi.openmodels.ResultMutable;
import eu.halo.openapi.openmodels.ResultResponse;
import eu.halo.service.ResultService;
import eu.halo.service.UserInfoService;

@RestController
@RequestMapping("/api")
public class ResultsController implements ResultsApi {

	private final ResultService resultService;

	public ResultsController(final ResultService resultService) {
		this.resultService = resultService;
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<ResultResponse> resultsResultIdDelete(String resultId) {
		ResultResponse resultResponse = new ResultResponse();
		resultResponse.setResult(ResultMapper.INSTANCE.mapEntityToApi(resultService.deleteResult(resultId)));
		resultResponse.setTimestamp(ResultMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<ResultResponse>(resultResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<ResultResponse> resultsResultIdGet(String resultId) {
		ResultResponse resultResponse = new ResultResponse();
		resultResponse.setResult(ResultMapper.INSTANCE.mapEntityToApi(resultService.getResult(resultId)));
		resultResponse.setTimestamp(ResultMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<ResultResponse>(resultResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<ResultResponse> resultsResultIdPut(String resultId, @Valid ResultMutable resultMutable) {
		ResultResponse resultResponse = new ResultResponse();
		resultResponse
				.setResult(ResultMapper.INSTANCE.mapEntityToApi(resultService.updateResult(resultId, resultMutable)));
		resultResponse.setTimestamp(ResultMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<ResultResponse>(resultResponse, HttpStatus.OK);
	}

}
