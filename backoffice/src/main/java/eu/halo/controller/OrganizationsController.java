package eu.halo.controller;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import eu.halo.openapi.OrganizationsApi;
import eu.halo.mapper.CountMapper;
import eu.halo.mapper.OrganizationMapper;
import eu.halo.mapper.SiteMapper;
import eu.halo.mapper.TagMapper;
import eu.halo.openapi.openmodels.CountResponse;
import eu.halo.openapi.openmodels.OrganizationMutable;
import eu.halo.openapi.openmodels.OrganizationResponse;
import eu.halo.openapi.openmodels.OrganizationsResponse;
import eu.halo.openapi.openmodels.SiteMutable;
import eu.halo.openapi.openmodels.SiteResponse;
import eu.halo.openapi.openmodels.SitesResponse;
import eu.halo.openapi.openmodels.TagsResponse;
import eu.halo.service.OrganizationService;
import eu.halo.service.SiteService;
import eu.halo.service.TagService;
import eu.halo.service.UserInfoService;
import eu.halo.service.util.ParametersChecker;

@RestController
@RequestMapping("/api")
public class OrganizationsController implements OrganizationsApi {

	private final OrganizationService organizationService;

	private final SiteService siteService;

	private final TagService tagService;

	public OrganizationsController(final OrganizationService organizationService, final SiteService siteService,
			final TagService tagService) {
		this.organizationService = organizationService;
		this.siteService = siteService;
		this.tagService = tagService;
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<CountResponse> organizationsCountGet() {
		return new ResponseEntity<CountResponse>(
				CountMapper.INSTANCE.mapCountToResponse(organizationService.getCount()), HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<OrganizationsResponse> organizationsGet(@Valid Integer offset, @Valid Integer limit) {
		OrganizationsResponse organizationsResponse = new OrganizationsResponse();
		if (ParametersChecker.checkOffsetAndLimit(offset, limit)) {
			organizationsResponse.setOrganizations(OrganizationMapper.INSTANCE
					.mapEntityToApi(organizationService.getOrganizations(offset.intValue(), limit.intValue())));
		} else {
			organizationsResponse.setOrganizations(OrganizationMapper.INSTANCE
					.mapEntityToApi(organizationService.getOrganizations(0, ParametersChecker.MAX_FETCH_LIMIT)));
		}
		organizationsResponse.setTimestamp(OrganizationMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<OrganizationsResponse>(organizationsResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<OrganizationResponse> organizationsOrganizationIdDelete(String organizationId) {
		OrganizationResponse organizationResponse = new OrganizationResponse();
		organizationResponse.setOrganization(
				OrganizationMapper.INSTANCE.mapEntityToApi(organizationService.deleteOrganization(organizationId)));
		organizationResponse.setTimestamp(OrganizationMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<OrganizationResponse>(organizationResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<OrganizationResponse> organizationsOrganizationIdGet(String organizationId) {
		OrganizationResponse organizationResponse = new OrganizationResponse();
		organizationResponse.setOrganization(
				OrganizationMapper.INSTANCE.mapEntityToApi(organizationService.getOrganization(organizationId)));
		organizationResponse.setTimestamp(OrganizationMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<OrganizationResponse>(organizationResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<OrganizationResponse> organizationsOrganizationIdPut(String organizationId,
			@Valid OrganizationMutable organizationMutable) {
		OrganizationResponse organizationResponse = new OrganizationResponse();
		organizationResponse.setOrganization(OrganizationMapper.INSTANCE
				.mapEntityToApi(organizationService.updateOrganization(organizationId, organizationMutable)));
		organizationResponse.setTimestamp(OrganizationMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<OrganizationResponse>(organizationResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<CountResponse> organizationsOrganizationIdSitesCountGet(String organizationId) {
		return new ResponseEntity<CountResponse>(
				CountMapper.INSTANCE.mapCountToResponse(siteService.getCount(organizationId)), HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<SitesResponse> organizationsOrganizationIdSitesGet(String organizationId,
			@Valid Integer offset, @Valid Integer limit) {
		SitesResponse sitesResponse = new SitesResponse();
		if (ParametersChecker.checkOffsetAndLimit(offset, limit)) {
			sitesResponse.setSites(SiteMapper.INSTANCE
					.mapEntityToApi(siteService.getSites(organizationId, offset.intValue(), limit.intValue())));
		} else {
			sitesResponse.setSites(SiteMapper.INSTANCE
					.mapEntityToApi(siteService.getSites(organizationId, 0, ParametersChecker.MAX_FETCH_LIMIT)));
		}
		sitesResponse.setTimestamp(SiteMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<SitesResponse>(sitesResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<SiteResponse> organizationsOrganizationIdSitesPost(String organizationId,
			@Valid SiteMutable siteMutable) {
		SiteResponse siteResponse = new SiteResponse();
		siteResponse.setSite(SiteMapper.INSTANCE.mapEntityToApi(siteService.createSite(organizationId, siteMutable)));
		siteResponse.setTimestamp(SiteMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<SiteResponse>(siteResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<TagsResponse> organizationsOrganizationIdTagsGet(String organizationId) {
		organizationService.getOrganization(organizationId);
		TagsResponse tagsResponse = new TagsResponse();
		tagsResponse.setTags(TagMapper.INSTANCE.mapEntityToApi(tagService.getTags(organizationId)));
		tagsResponse.setTimestamp(TagMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<TagsResponse>(tagsResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<OrganizationResponse> organizationsPost(@Valid OrganizationMutable organizationMutable) {
		OrganizationResponse organizationResponse = new OrganizationResponse();
		organizationResponse.setOrganization(OrganizationMapper.INSTANCE
				.mapEntityToApi(organizationService.createOrganization(organizationMutable)));
		organizationResponse.setTimestamp(OrganizationMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<OrganizationResponse>(organizationResponse, HttpStatus.OK);
	}

}
