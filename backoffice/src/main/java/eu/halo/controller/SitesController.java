package eu.halo.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import eu.halo.openapi.openmodels.CountResponse;
import eu.halo.openapi.openmodels.ResultMutable;
import eu.halo.openapi.openmodels.ResultResponse;
import eu.halo.openapi.openmodels.ResultsResponse;
import eu.halo.openapi.openmodels.SiteMutable;
import eu.halo.openapi.openmodels.SiteResponse;
import eu.halo.mapper.CountMapper;
import eu.halo.mapper.ResultMapper;
import eu.halo.mapper.SiteMapper;
import eu.halo.openapi.SitesApi;
import eu.halo.service.ResultService;
import eu.halo.service.SiteService;
import eu.halo.service.UserInfoService;
import eu.halo.service.util.ParametersChecker;

@RestController
@RequestMapping("/api")
public class SitesController implements SitesApi {

	private final SiteService siteService;

	private final ResultService resultService;

	public SitesController(final SiteService siteService, final ResultService resultService) {
		this.siteService = siteService;
		this.resultService = resultService;
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<SiteResponse> sitesSiteIdDelete(String siteId) {
		SiteResponse siteResponse = new SiteResponse();
		siteResponse.setSite(SiteMapper.INSTANCE.mapEntityToApi(siteService.deleteSite(siteId)));
		siteResponse.setTimestamp(SiteMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<SiteResponse>(siteResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<SiteResponse> sitesSiteIdGet(String siteId) {
		SiteResponse siteResponse = new SiteResponse();
		siteResponse.setSite(SiteMapper.INSTANCE.mapEntityToApi(siteService.getSite(siteId)));
		siteResponse.setTimestamp(SiteMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<SiteResponse>(siteResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<SiteResponse> sitesSiteIdPut(String siteId, @Valid SiteMutable siteMutable) {
		SiteResponse siteResponse = new SiteResponse();
		siteResponse.setSite(SiteMapper.INSTANCE.mapEntityToApi(siteService.updateSite(siteId, siteMutable)));
		siteResponse.setTimestamp(SiteMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<SiteResponse>(siteResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE, UserInfoService.USER_ROLE })
	public ResponseEntity<CountResponse> sitesSiteIdResultsCountGet(String siteId) {
		return new ResponseEntity<CountResponse>(
				CountMapper.INSTANCE.mapCountToResponse(resultService.getCount(siteId)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResultsResponse> sitesSiteIdResultsGet(String siteId, @Valid Integer offset,
			@Valid Integer limit, @Valid String tag, @Valid List<String> sort) {
		ResultsResponse resultsResponse = new ResultsResponse();
		if (ParametersChecker.checkOffsetAndLimit(offset, limit)) {
			if (ParametersChecker.checkTag(tag)) {
				resultsResponse.setResults(ResultMapper.INSTANCE.mapEntityToApi(
						resultService.getResults(siteId, offset.intValue(), limit.intValue(), tag, sort)));
			} else {
				resultsResponse.setResults(ResultMapper.INSTANCE
						.mapEntityToApi(resultService.getResults(siteId, offset.intValue(), limit.intValue(), sort)));
			}
		} else {
			if (ParametersChecker.checkTag(tag)) {
				resultsResponse.setResults(ResultMapper.INSTANCE.mapEntityToApi(
						resultService.getResults(siteId, 0, ParametersChecker.MAX_FETCH_LIMIT, tag, sort)));
			} else {
				resultsResponse.setResults(ResultMapper.INSTANCE
						.mapEntityToApi(resultService.getResults(siteId, 0, ParametersChecker.MAX_FETCH_LIMIT, sort)));
			}

		}
		resultsResponse.setTimestamp(ResultMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<ResultsResponse>(resultsResponse, HttpStatus.OK);
	}

	@Override
	@RolesAllowed({ UserInfoService.ADMIN_ROLE })
	public ResponseEntity<ResultResponse> sitesSiteIdResultsPost(String siteId, @Valid ResultMutable resultMutable) {
		ResultResponse resultResponse = new ResultResponse();
		resultResponse
				.setResult(ResultMapper.INSTANCE.mapEntityToApi(resultService.createResult(siteId, resultMutable)));
		resultResponse.setTimestamp(ResultMapper.INSTANCE.getFormattedOffsetTimestamp());
		return new ResponseEntity<ResultResponse>(resultResponse, HttpStatus.OK);
	}

}
