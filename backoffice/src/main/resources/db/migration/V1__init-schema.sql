CREATE TABLE organization (
id uuid NOT NULL PRIMARY KEY,
organization VARCHAR(255) NOT NULL,
created TIMESTAMP NOT NULL,
updated TIMESTAMP
);

CREATE TABLE site (
id uuid NOT NULL PRIMARY KEY,
site VARCHAR(255) NOT NULL,
created TIMESTAMP NOT NULL,
updated TIMESTAMP,
num BIGINT NOT NULL,
min DOUBLE PRECISION,
max DOUBLE PRECISION,
avg DOUBLE PRECISION,
organization_id uuid NOT NULL
);

CREATE TABLE result (
id uuid NOT NULL PRIMARY KEY,
created TIMESTAMP NOT NULL,
contributor VARCHAR(255) NOT NULL,
device VARCHAR(255) NOT NULL,
cartridge VARCHAR(255) NOT NULL,
batch VARCHAR(255),
result DOUBLE PRECISION NOT NULL,
comment VARCHAR(1000),
site_id uuid NOT NULL
);

CREATE TABLE tag (
id uuid NOT NULL PRIMARY KEY,
tag VARCHAR(255) NOT NULL
);

CREATE TABLE result_tag (
result_id uuid NOT NULL,
tag_id uuid NOT NULL,
PRIMARY KEY (result_id, tag_id)
);

ALTER TABLE IF EXISTS result_tag
ADD CONSTRAINT fk_tag FOREIGN KEY (tag_id) REFERENCES tag;

ALTER TABLE IF EXISTS result_tag
ADD CONSTRAINT fk_result FOREIGN KEY (result_id) REFERENCES result;

ALTER TABLE IF EXISTS result
ADD CONSTRAINT fk_site FOREIGN KEY (site_id) REFERENCES site;

ALTER TABLE IF EXISTS site
ADD CONSTRAINT fk_organization FOREIGN KEY (organization_id) REFERENCES organization;

ALTER TABLE IF EXISTS organization
ADD CONSTRAINT unique_organization UNIQUE (organization);

ALTER TABLE IF EXISTS site
ADD CONSTRAINT unique_site UNIQUE (site);
