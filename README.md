# halo-backoffice

Backend part of the h-ALO project. Consists of following custom components:
- Backoffice - REST API implementation
- Postgres - Database
- Keycloak - Identity provider
- Superset - Data visualisation tool
- Celery - Task queue with data processing worker

We also use two standard images for:
- Minio
- RabbitMQ

# Brief instruction

To setup development environment it's enough to run all build*.sh scripts and then start docker-compose.
> ./buildBackoffice.sh
> ./buildCelery.sh
> ./buildKeycloak.sh
> ./buildPostgres.sh
> ./buildSuperset.sh
> ./dockerUp.sh

Use loadTestdata.sh to load some entities to DB.
> ./loadTestdata.sh

# Deploy

In deploy subdirectory there are some more info/tools for test/production deployment. See README there.

# Tests

In test subdirectory there are currently two things:
- postman - Postman collection for calling our Backoffice API
- python - Python scripts for testing chosen features
