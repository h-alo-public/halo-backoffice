CREATE DATABASE keycloak;
CREATE USER keycloak WITH PASSWORD 'keycloak';
GRANT ALL PRIVILEGES ON DATABASE keycloak TO keycloak;

CREATE DATABASE backoffice;
CREATE USER backoffice WITH PASSWORD 'backoffice';
GRANT ALL PRIVILEGES ON DATABASE backoffice TO backoffice;

CREATE DATABASE superset;
CREATE USER superset WITH PASSWORD 'superset';
GRANT ALL PRIVILEGES ON DATABASE superset TO superset;
