#/bin/bash
export PGPASSWORD=backoffice
psql -h localhost -p 5433 -d backoffice -U backoffice --no-password -f ./postgres/testdata/postgresql-halo-insert-data.sql
psql -h localhost -p 5433 -d backoffice -U backoffice --no-password -f ./postgres/testdata/postgresql-halo-insert-data-tags.sql
